//TFG Alejandro Ortiz - IEC UCM
//EPS-UCM V1

/*
 * El siguiente código está diseñado para poder ser usado en un Arduino Mega
 * o en un Arduino Nano mediante la modificación de los pines destinados a
 * la generación de la señal PWM del convertidor SEPIC y al calentador de la
 * batería. Por otro lado, será necesario la modificación de los timers como
 * se describe en el apartado "Timers".
 * 
 * El software está diseñado para una corriente de carga de 200 mA.
 * El modo de corriente constante se iniciará si la tensión de la batería
 * se sitúa por debajo de 8.05 V. En ese punto, será cuando se inicie el 
 * modo de tensión constante, manteniendo un nivel de tensión de 8.05 V. 
 * 
 * Por otro lado, el sistema implementa un sensor de temperatura TMP36,
 * que funciona con un voltaje de alimentación de 5V como está indicado
 * en la funcion get_temp. La tierra (GND) para el sensor será el puerto
 * GND cercano a AREF, ya que si no, se observarán irregularidades en la
 * lectura de voltaje, obteniendo valores incorrectos de temperatura.
*/

//------------------------- Librerías -------------------------
#include <Wire.h>
#include <Adafruit_INA219.h>
#include <SDL_Arduino_INA3221.h>

//------------------------- Sensor INA219 -------------------------
Adafruit_INA219 ina219_bat(0x44);
Adafruit_INA219 ina219_pv(0x41);

//------------------------- Sensor INA3221 -------------------------
SDL_Arduino_INA3221 ina3221;
#define LOAD_5V 1
#define LOAD_8V4 2
#define LOAD_3V3 3

//------------------------- Constantes -------------------------
const float vbat_cv = 8.05;       //Voltaje de inicio de voltaje constante
const float cc_charge = 200;      //Corriente de carga en modo CC (Corriente Constante)
int set_temp = 40;                //Temperatura fijada para la batería
const float vbat_min = 7;      //Voltaje de desactivación de cargas si la batería no se carga
const float vpv_min = 3.3;        //Voltaje de desactivación de fuente de alimentación

//------------------------- Pines analógicos -------------------------
const int vpv_pin = A0;           //Pin de fuente de alimentación
const int temp_pin = A1;          //Pin de sensor TMP36
const int vbat_pin = A2;          //Pin de batería
const int cell_pin = A3;          //Pin para la celda de batería número 1

//------------------------- Salidas -------------------------
const int pwm_sepic = 6;          //D6 Pin  Mega: 6, Nano: 10
const int pwm_heater = 9;         //D9 Pin Mega: 9, Nano: 9
const int load1_ctrl = 3;         //5V Load   Mega: 3, Nano: 3    5V
const int load2_ctrl = 5;         //8V4 Load  Mega: 5, Nano: 5    8V4
const int load3_ctrl = 4;         //3V3 Load  Mega: 4, Nano: 4    3V3

//------------------------- Variables -------------------------
float solar_current = 0.0;        //Corriente de panel solar
float solar_voltage = 0.0;        //Voltaje de panel solar
float vbat = 0.0;                 //Voltaje total de la batería (vbat = vcell_1 + vcell_2)
float vcell_1 = 0.0;              //Variable para almacenar el voltaje de la celda 1
float vcell_2 = 0.0;              //Variable para almacenar el voltaje de la celda 2
float load_ch1_current = 0.0;     //Variable para almacenar la corriente de la carga 5V
float load_ch2_current = 0.0;     //Variable para almacenar la corriente de la carga 8V4
float load_ch3_current = 0.0;     //Variable para almacenar la corriente de la carga 3V3    
float bat_current = 0.0;          //Variable para almacenar el valor de la corriente de la batería
float SoC = 0.0;                  //Variable para almacenar el valor de SoC
int pwm_value = 0;                //Variable para la señal PWM del convertidor SEPIC
int pwm_temp = 0;                 //Variable para la señal PWM del battery heater
float temp = 0.0;                 //Variable para almacenar el valor de temperatura
int flag_1 = 0;                   //Flag para la función de Stop/Continue 
int flag_ac = 0;                  //Flag para Arduino Control del convertidor SEPIC
int flag_bc = 0;                  //Flag para detectar si la batería puede alimentar al sistema

//String
String command;

//------------------------- SETUP ------------------------------------
void setup() {

  pinMode(pwm_sepic, OUTPUT); 
  pinMode(pwm_heater, OUTPUT);
  pinMode(load1_ctrl, OUTPUT);
  pinMode(load2_ctrl, OUTPUT);
  pinMode(load3_ctrl, OUTPUT);
  
  //digitalWrite(pwm_sepic, LOW); //Set PWM to HIGH so buck converter will start at lower value
  digitalWrite(load1_ctrl, HIGH);
  digitalWrite(load2_ctrl, HIGH);
  digitalWrite(load3_ctrl, HIGH);

  //Timers
  //Arduino Mega
  TCCR2B = TCCR2B & B11111000 | B00000001;  //Timer 2 para configurar la frecuencia de la señal PWM a 31372.55 Hz para los pines 9 y 10 del Arduino Mega
  TCCR4B = TCCR4B & B11111000 | B00000001;  //Timer 4 para configurar la frecuencia de la señal PWM a 31372.55 Hz para los pines 6,7 y 8 del Arduino Mega
  
  //Arduino Nano (descomentar si se usa)
  /*TCCR1B = TCCR1B & B11111000 | B00000001;  //Timer 1 para configurar la frecuencia de la señal PWM a 31372.55 Hz para los pines 9 y 10 del Arduino Nano
  //SDA: A4(amarillo), SCL: A5(naranja)*/

  //INA219 Init
  uint32_t currentFrequency;
  // La calibración por defecto del INA219 es de 32V y 2A.
  // Para usar la calibración de 32V y 1A (con mayor precision de Amperios):
  // ina219.setCalibration_32V_1A();
  // Para usar la calibración de 16V y 400mA (con mayor precisión en V y A):
  // ina219.setCalibration_16V_400mA();
  ina219_bat.begin();
  ina219_pv.begin();

  //INA3221 Init
  ina3221.begin();

  Serial.begin(115200);

  delay(500);
  Serial.println("Introduce uno de los siguientes comandos:");
  Serial.println("- L1 on: Activa el Load 1");
  Serial.println("- L1 off: Desactiva el Load 1");
  Serial.println("- L2 on: Activa el Load 2");
  Serial.println("- L2 off: Desactiva el Load 2");
  Serial.println("- L3 on: Activa el Load 3");
  Serial.println("- L3 off: Desactiva el Load 3");
  Serial.println("- Loads: Corriente de cada carga");
  Serial.println("- L on: Activa todas las cargas");
  Serial.println("- L off: Desactiva todas las cargas");
  Serial.println("- Battery status: Muestra información de la batería");
  Serial.println("- Pv status: Muestra información de la fuente de alimentación");
  Serial.println("- Duty: Muestra el ciclo de trabajo del convertidor SEPIC");
  Serial.println("- S: Detiene el bucle");
  Serial.println("- C: Reanuda el bucle");
}

//------------------------- LOOP ------------------------------------
void loop() {
  //Valores de fuente de alimentación
  solar_voltage = get_solar_voltage(10);
  solar_current = get_solar_current(10);

  //Estado de batería
  vbat = get_battery_voltage(10);
  vcell_1 = cell_voltage(10);
  vcell_2 = vbat - vcell_1; 
  bat_current = get_battery_current(10);

  //Corriente de cada carga
  load_ch1_current = get_load1_current(10);
  load_ch2_current = get_load2_current(10);
  load_ch3_current = get_load3_current(10);

  //SoC (State of Charge)
  SoC = measure_soc(vbat);

  //Adquisicón de valor de temperatura
  temp = get_temp(10);
  
  pwm_temp = temp_control(temp, set_temp);
  analogWrite(pwm_heater, pwm_temp);

  //print_values();
  
  //Proceso de carga de la batería
  //Seleccion de modo
  //Si el voltaje es inferior a vbat_cv = 8.05V, se activa el modo de corriente constante
  if ((vbat < vbat_cv)){         
    //Modo Corriente Constante
    pwm_value = battery_cc(bat_current,cc_charge, flag_ac);
    analogWrite(pwm_sepic, pwm_value);
  /*Si el voltaje es igual o superior a vbat_cv = 8.05V, se activa el modo de voltaje constante
   * Además, si el voltaje de cada celda es superior a 4 V, también se activa este modo
  */
  } else if (vbat >= vbat_cv || vcell_1 > 4 || vcell_2 > 4) {
    //Modo Voltaje Constante (VC)
    pwm_value = battery_cc(vbat,vbat_cv, flag_ac);
    analogWrite(pwm_sepic, pwm_value);
    }

  //CC Mode
   pwm_value = battery_cc(bat_current,cc_charge, flag_ac);
   analogWrite(pwm_sepic, pwm_value);
    
  flag_ac = arduino_control(solar_voltage);       //Flag para el control del convertidor SEPIC (si es menor de un valor, la señal PWM será del 25% del ciclo de trabajo
  //flag_bc = battery_control(solar_voltage, vbat, flag_ac);       //Flag para desactivar las cargas si la batería es inferior al valor de tensión vbat_min 

  //Lectura por pantalla
  write_command();    //Se utiliza para introducir comandos
 
  delay(10);
}//End void loop


//------------------------- FUNCTIONS ------------------------------------
void print_values(){
  /*Serial.print("Solar Voltage: "); Serial.print(solar_voltage); Serial.println(" V");
  Serial.print("Battery Voltage: "); Serial.print(vbat); Serial.println(" V");
  Serial.print("Battery Current: "); Serial.print(bat_current); Serial.println(" mA");
  Serial.print("Cell Voltage: "); Serial.print(vcell_1); Serial.println(" V");
  Serial.print("Solar Current: "); Serial.print(solar_current); Serial.println(" mA");
  /*Serial.print("Load 1 Current: "); Serial.print(load_ch1_current); Serial.println(" mA");
  Serial.print("Load 2 Current: "); Serial.print(load_ch2_current); Serial.println(" mA");
  Serial.print("Load 3 Current: "); Serial.print(load_ch3_current); Serial.println(" mA");*/
  Serial.print("Temperature is: "); Serial.print(temp); Serial.println(" ºC");
  //Serial.print("SOC is: "); Serial.print(SoC); Serial.println("%");
  Serial.println("------------------------------------------------------------------");
}

void write_command(){
  if(Serial.available()){
    command = Serial.readStringUntil('\n');
    command.trim();
    //Loads control
    if(command.equals("L1 on")){          
        digitalWrite(load1_ctrl, HIGH);
        Serial.println("Load 5V ON");
    } else if(command.equals("L1 off")){
        digitalWrite(load1_ctrl, LOW);
        Serial.println("Load 5V OFF");
    } else if(command.equals("L2 on")){
        digitalWrite(load2_ctrl, HIGH);
        Serial.println("Load 8V4 ON");
    } else if(command.equals("L2 off")){
        digitalWrite(load2_ctrl, LOW);
        Serial.println("Load 8V4 OFF"); 
    } else if(command.equals("L3 on")){
        digitalWrite(load3_ctrl, HIGH);
        Serial.println("Load 3V3 ON");
    } else if(command.equals("L3 off")){
        digitalWrite(load3_ctrl, LOW);
        Serial.println("Load 3V3 OFF"); 
    } else if(command.equals("Loads")){
      Serial.print("Load 5V: ");    Serial.print(load_ch1_current);   Serial.println(" mA");
      Serial.print("Load 8V4: ");   Serial.print(load_ch2_current);   Serial.println(" mA");
      Serial.print("Load 3V3: ");   Serial.print(load_ch3_current);   Serial.println(" mA");
    } else if(command.equals("L on")){
      digitalWrite(load1_ctrl, HIGH);
      digitalWrite(load2_ctrl, HIGH);
      digitalWrite(load3_ctrl, HIGH);
      Serial.println("All loads ON");
    } else if(command.equals("L off")){
      digitalWrite(load1_ctrl, LOW);
      digitalWrite(load2_ctrl, LOW);
      digitalWrite(load3_ctrl, LOW);
      Serial.println("All loads OFF");
      
    //Estado de la bateria
    } else if(command.equals("Battery status")){
      Serial.println("Battery Status");
      Serial.print("Current: ");  Serial.print(bat_current);  Serial.println(" mA");
      Serial.print("Pack Voltage: ");  Serial.print(vbat);   Serial.println(" V");
      Serial.print("Voltage Cell 1: ");  Serial.print(vcell_1);   Serial.println(" V");
      Serial.print("Voltage Cell 2: ");  Serial.print(vcell_2);   Serial.println(" V");
      Serial.print("SoC: ");   Serial.print(SoC);   Serial.println(" %");
      Serial.print("Temperature: ");   Serial.print(temp);   Serial.println(" ºC");

    } else if(command.equals("Temp")){
      Serial.print("Temperature: ");   Serial.print(temp);   Serial.println(" ºC");

    //Estado de fuente de alimentación
    } else if(command.equals("Pv status")){
      Serial.print("Solar panel: ");  Serial.print(solar_voltage);  Serial.print(" V and ");   Serial.print(solar_current);    Serial.println(" mA");
      
    //Ciclo de trabajo
    } else if(command.equals("Duty")){
      Serial.print("PWM SEPIC: ");  Serial.print((pwm_value*100)/255);   Serial.println(" %");
    //El programa parará
    } else if(command.equals("S")){
        flag_1 = 1;
        while(flag_1 == 1){
          if(Serial.available()){
          command = Serial.readStringUntil('\n');
          command.trim();
            if(command.equals("C")){    //El programa continuará su bucle
              flag_1 = 0;
            }
          }
        }
    }
  }
}

//Arduino ON/OFF control
int arduino_control(float vpv){
  float shuntvoltage_bat = 0;
  float busvoltage_bat = 0;
  float loadvoltage_bat = 0;
 
  shuntvoltage_bat = ina219_bat.getShuntVoltage_mV();
  busvoltage_bat = ina219_bat.getBusVoltage_V();
  loadvoltage_bat = busvoltage_bat + (shuntvoltage_bat / 1000);
  
  if(vpv < vpv_min){
    pwm_value = 64;
    flag_ac = 1;
  } else {
    flag_ac = 0; 
  }
  return(flag_ac);
}

//Battery control
/*
 * Esta función sirve para identificar que la batería no tiene
 * un voltaje aceptado para potenciar al sistema. En este estado
 * las cargas se abrirán y así se evitará la descarga de la batería.
 */
int battery_control(float vpv, float vbat_bc, int flag_ac){
  if(((vpv < 5) || (vbat_bc < vbat_min)) && (flag_ac = 1)){
    /*
     * Desactiva las tres cargas si el voltaje de la batería es 
     * menor que 7 (~5% SoC). Modifica el flag_bc a valor 1. 
     */
    digitalWrite(load1_ctrl, LOW);
    digitalWrite(load2_ctrl, LOW);
    digitalWrite(load3_ctrl, LOW);
    flag_bc = 1; 
  } else {
    flag_bc = 0; 
  }
  return(flag_bc); 
}

//Battery Charge Control (battery_cc)
int battery_cc(float bat_voltage, float bat_min_voltage, int flag_ac){ //vbat, vbat_max
  //Control de la señal PWM para convertidor SEPIC
  if (flag_ac == 0){
    if (bat_voltage < bat_min_voltage) {
      pwm_value++;
      pwm_value = constrain(pwm_value, 15, 240);
    } else if (bat_voltage >= bat_min_voltage) {
      pwm_value--;
      pwm_value = constrain(pwm_value, 15, 240);
    }
  } else if (flag_ac == 1){
    pwm_value = pwm_value; 
  }
  return(pwm_value);
}

//Control PWM de temperatura 
int temp_control(float actual_temp, int set_temperature){
    if (actual_temp > set_temperature) {
      pwm_temp--;
      pwm_temp = constrain(pwm_temp, 15, 240);
    } else {
      pwm_temp++;
      pwm_temp = constrain(pwm_temp, 15, 240);
    }
  return(pwm_temp);
}

//Medida SoC
float measure_soc(float bat_soc){
  SoC = ((bat_soc-6.55)/(8.4-6.55))*100;
  if (SoC < 0){
    SoC = 0;
  }
  return(SoC);
}
float get_solar_voltage(int n_samples){
  float voltage = 0;
  for (int i = 0; i < n_samples; i++)
  {
    voltage += (analogRead(vpv_pin) * (5 / (1024.0 * (7320.0 / 36720.0))))-0.32;
  }
  voltage = voltage / n_samples;
  if (voltage < 0) {
    voltage = 0;
  }
  return (voltage);
}

float get_battery_voltage(int n_samples) {
  float voltage = 0.0;
  for (int i = 0; i < n_samples; i++){
    voltage += (analogRead(vbat_pin) * (5.0 / (1024.0 *(7440.0 / (29600.0 + 7440.0)))))-0.32;//(7440.0 / (29600.0 + 7440.0)). 0.32 = Offset
  }
  voltage = voltage / n_samples;
  if (voltage < 0) {
    voltage = 0;
  }
  return (voltage);
}

float cell_voltage(int n_samples) {
  float cell_v = 0.0;
  for (int i = 0; i < n_samples; i++){
    cell_v += (analogRead(cell_pin) * (5.0 / (1024.0 *(7440.0 / (29600.0 + 7440.0)))));//(7440.0 / (29600.0 + 7440.0)). 0.32 = Offset
  }
  cell_v = cell_v / n_samples;
  if (cell_v < 0) {
    cell_v = 0;
  }
  return (cell_v);
}

float get_battery_current(int n_samples){
  float battery_current = 0.0;
  float current_mA = 0.0;
  for (int i = 0; i < n_samples; i++) {
    current_mA = ina219_bat.getCurrent_mA();
    battery_current = battery_current + current_mA;
  }
  battery_current = battery_current / n_samples;
  return (battery_current);
}

float get_solar_current(int n_samples){
  float solar_current = 0.0;
  float current_mA = 0.0;
  for (int i = 0; i < n_samples; i++) {
    current_mA = ina219_pv.getCurrent_mA();
    solar_current = solar_current + current_mA;
  }
  solar_current = solar_current / n_samples;
  if (solar_current < 0) {
    solar_current = 0;
  }
  return (solar_current);
}

float get_load1_current(int n_samples){
  float load1_current = 0.0;
  float current_mA = 0.0;
  for (int i=0; i<n_samples; i++){
    current_mA = ina3221.getCurrent_mA(LOAD_5V);
    load1_current = load1_current + current_mA;
  }
  load1_current = load1_current/n_samples;
  if(load1_current < 0){
    load1_current = 0;
  }
  return(load1_current);
}

float get_load2_current(int n_samples){
  float load2_current = 0.0;
  float current_mA = 0.0;
  for (int i=0; i<n_samples; i++){
    current_mA = ina3221.getCurrent_mA(LOAD_8V4);
    load2_current = load2_current + current_mA;
  }
  load2_current = load2_current/n_samples;
  if(load2_current < 0){
    load2_current = 0;
  }
  return(load2_current);
}

float get_load3_current(int n_samples){
  float load3_current = 0.0;
  float current_mA = 0.0;
  for (int i=0; i<n_samples; i++){
    current_mA = ina3221.getCurrent_mA(LOAD_3V3);
    load3_current = load3_current + current_mA;
  }
  load3_current = load3_current/n_samples;
  if(load3_current < 0){
    load3_current = 0;
  }
  return(load3_current);
}

float get_temp(int n_samples) {
  
  float temp_read = 0.0;
  float temp_mV = 0.0;

  for (int i = 0; i < n_samples; i++){
    temp_mV = analogRead(temp_pin)*5.0;
    temp_mV /= 1024.0;
    
    temp_read += ((temp_mV-0.5)*100);  //Lectura de voltaje del sensor TMP36
  }
  temp_read = temp_read / n_samples;  //Media de lectura
  return (temp_read);                 //Retorno del valor adquirido
}
//END Functions Loop
